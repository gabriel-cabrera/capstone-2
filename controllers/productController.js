const User = require("../models/userModel");
const Product = require("../models/productModel");
const Order = require("../models/orderModel");
const mongoose = require("mongoose");

// Adding product
module.exports.addProduct = (data) => {

	if (data.isAdmin){

		let newProduct = new Product({

			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			image : data.product.image
		});

		return newProduct.save().then((newProduct, error) => {
			if(error) {
				return false
			} 
				return {
					message : `${newProduct.name} successfully added!`
				}			
		})
	}

		let message = Promise.resolve('User must be Admin to add a new product.')

		return message.then((value) => {
			return value
		})
}

// Retrieving All Products (Admin only)

module.exports.getAllProducts = (data) =>{

	if(data.isAdmin){
		return Product.find().then(result =>{
			return result;
		})
	} let message = Promise.resolve('User must be an Admin to get all products.')
	return message.then((value)=> {
		return value
	})
}

// Retrieving All Active Products

module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Retrieving Single Product

module.exports.getSingleProduct = (data) => {

	return Product.findById(data.productId).then((result, error) => {

			return result;
		
		})
}

// Updating Product

module.exports.updateProduct = (productId, data) => {


		if(data.isAdmin){

			let updatedProduct = {
			name : data.update.name,
			description : data.update.description,
			price : data.update.price,
			lastUpdated : new Date()
		}

		return Product.findByIdAndUpdate(productId, updatedProduct).then((result, error) =>{

			if(error){
				console.log(error);
				return false;
			} 
				
				result = updatedProduct

				console.log(productId)
				return result 

			})
		} 
			let message = Promise.resolve("Only Admins can update product details!")

			return message.then((value) => {
				return value
			})
}

	
// Archiving / Activating Products 

module.exports.archive = (productId, data) => {

	if(data){

		let updatedStatus = {

			isActive : false
		};

		return Product.findByIdAndUpdate(productId, updatedStatus).then((result, error) =>{

			if(error){
				console.log(error);
				return false;
			}

				result = updatedStatus

				console.log(productId)
				return {
					
					productId,
					result

				} 
		})
	}
		let message = Promise.resolve("Only Admins can update product status!")

			return message.then((value) => {
				return value

				})
}
	
module.exports.unarchive = (productId, data) => {

	if(data){

		let updatedStatus = {

			isActive : true
		};

		return Product.findByIdAndUpdate(productId, updatedStatus).then((result, error) =>{

			if(error){
				console.log(error);
				return false;
			}

				result = updatedStatus

				console.log(productId)
				return {
					
					productId,
					result

				} 
		})
	}
		let message = Promise.resolve("Only Admins can update product status!")

			return message.then((value) => {
				return value

				})
}