const User = require("../models/userModel");
const Product = require("../models/productModel");
const Order = require("../models/orderModel");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.checkEmailExists = (reqBody) => {

  return User.find({email : reqBody.email}).then(result => {

    // the find method returns a record if a match is found

    if (result.length > 0) {

      return true
    }

    return false
  })
}
  
// Registration

module.exports.registerUser = async (reqBody) => {
  const existingUser = await User.findOne({ email: reqBody.email });

  if (existingUser) {
    return "User already exists";
  }

  let newUser = new User({
    firstName : reqBody.firstName,
    lastName : reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    isAdmin: reqBody.isAdmin,
  });

  return newUser
  .save()
  .then((user) => {
    return user;
  })
  .catch((error) => {
    return "Error creating user: " + error;
  });
};


// Login

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null){

			return {

				message : "User does not exist"
			}
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return {access : auth.createAccessToken(result)}
			} else {

				return {

					message : "Password is Incorrect"
				}
			};
		};
	});
};

//Check out

module.exports.checkOut = async (orderId, data) => {

	let findUser = await User.findById(data.userId);

	let order = await Order.findById(orderId);
  let product = await order.products;

	if(findUser.isAdmin !== true){

		findUser.orders.push({
      orderId : orderId,
      products : order.products,
      totalAmount : order.totalAmount
    });

		return findUser.save();

	} else {

		return {
			message : "Admin go away!"
		}
	}
}

// Get user details

module.exports.getUser = (data) => {
  return User.findOne({_id : data.id}).then(result => {
    if(result == null){
      return false
    } else {
    result.password = ""
		return result;
    }
	});
};

// Get all users

module.exports.getAllUser = async (data) => {

  let findUser = await User.findById(data.userId);

  if(findUser.isAdmin){

    return User.find().then(result =>{
      return result;
    });

  }   let message = Promise.resolve('Only admins have access to this')

    return message.then((value)=> {
    return value;
    });
};


// Set Admin

module.exports.setAdmin = async (userId, data) => {

  let findUser = await User.findById(data.admin);

	if(findUser.isAdmin){

		let updatedStatus = {

			isAdmin : data.update.isAdmin
		};

		return User.findByIdAndUpdate(userId, updatedStatus).then((result, error) =>{

			if(error){
				console.log(error);
				return false;
			}

				result = updatedStatus

				console.log(userId)
				return {
					
					userId,
					result

				} 
		})
	} else{

		let message = Promise.resolve("Only Admins can access this!")

			return message.then((value) => {
				return value

				})
	}
}

// Get Orders

module.exports.getOrders = async (data) => {

	 let findUser = await User.findById(data.userId);

  if(findUser.isAdmin){

    let message = Promise.resolve('Admins do have orders')

    return message.then((value)=> {
    return value;
    });

  } return User.findById(data.userId).then(result =>{
      return result.orders;
    });
};

// add to cart

module.exports.addToCart = async (data) => {
  let findProduct = await Product.findById(data.productId);
  let findUser = await User.findById(data.userId);

  if (findUser.isAdmin !== true) {
    if (findUser.cart.length === 0) {
      findUser.cart.push({
        products: [
          {
            productId: findProduct.id,
            quantity: data.quantity,
            subTotal: data.quantity * findProduct.price,
          },
        ],
        totalAmount: data.quantity * findProduct.price,
      });
    } else {
      let currentCart = findUser.cart[0];
      let existingProductIndex = currentCart.products.findIndex(
        (product) => product.productId === data.productId
      );

      if (existingProductIndex === -1) {
        currentCart.products.push({
          productId: findProduct.id,
          quantity: data.quantity,
          subTotal: data.quantity * findProduct.price,
        });
      } else {
        currentCart.products[existingProductIndex].quantity +=
          data.quantity;
        currentCart.products[existingProductIndex].subTotal +=
          data.quantity * findProduct.price;
      }

      currentCart.totalAmount = currentCart.products.reduce(
        (acc, product) => acc + product.subTotal,
        0
      );
    }
  } else {
    return {
      message: "Admins are not allowed to add products to cart!",
    };
  }

  await findUser.save();
  return {message : "Cart has been updated!"}
};

// Changing Product Quantity

module.exports.changeQuantity = async (data) => {
  let findUser = await User.findById(data.userId);

  let findProduct = await Product.findById(data.productId);

  if (findUser.isAdmin !== true) {
    let targetCart;
    let targetProductIndex;

    findUser.cart.forEach((cart, index) => {
      let productIndex = cart.products.findIndex(
        product => product.productId === data.productId
      );
      if (productIndex !== -1) {
        targetCart = cart;
        targetProductIndex = productIndex;
      }
    });

    if (!targetCart) {
      return { message: "Product not found in the cart" };
    }

    let updatedProduct = targetCart.products[targetProductIndex];
    updatedProduct.quantity = data.quantity;
    updatedProduct.subTotal = findProduct.price * updatedProduct.quantity;


    targetCart.totalAmount = targetCart.products.reduce((acc, product) => acc + product.subTotal, 0);
    await findUser.save();

    return { message: `Quantity for product ${data.productId} updated` };
  } else {
    return { message: "Admin Go Away!" };
  }
};

// remove from cart

module.exports.removeFromCart = async (data) => {
  let findUser = await User.findById(data.userId);

  if (findUser.isAdmin !== true) {
    let targetCart;
    let targetProductIndex;

    findUser.cart.forEach((cart, index) => {
      let productIndex = cart.products.findIndex(
        product => product.productId === data.productId
      );
      if (productIndex !== -1) {
        targetCart = cart;
        targetProductIndex = productIndex;
      }
    });

    if (!targetCart) {
      return { message: "Product not found in the cart" };
    } 

    targetCart.products.splice(targetProductIndex, 1);

    targetCart.totalAmount = targetCart.products.reduce((acc, product) => acc + product.subTotal, 0);
    await findUser.save();

    return { message: `Product with ID ${data.productId} removed from the cart` };
  } else {
    return { message: "Admin Go Away!" };
  }
};