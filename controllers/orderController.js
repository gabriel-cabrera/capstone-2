const User = require("../models/userModel");
const Product = require("../models/productModel");
const Order = require("../models/orderModel");



// Creating Order

module.exports.createOrder = async (product, data) => {

	let findProduct = await Product.findById(product);

	let findUser = await User.findById(data.userId);

	if(findUser.isAdmin !== true){

		let newOrder = new Order({

			userId : findUser.id,
			products : [{
				productId : findProduct.id,
				name : findProduct.name,
				price : findProduct.price}],
			totalAmount : findProduct.price
		}) 	
			return newOrder.save(); 

	} else {

		return {
			message : "Admins are not allowed to create order!"
		}
	}

}

// Get all user orders

module.exports.getUserOrders = (data) => {
  return new Promise((resolve, reject) => {
    const findUser = User.findOne({_id : data.id});
    findUser.exec((err, user) => {
      if (err) {
        reject(err);
      } else {
        resolve(user.orders);
      }
    });
  });
};

// Get all orders

module.exports.getAllOrders = async (data) => {

	let findUser = await User.findById(data.userId);

	if(findUser.isAdmin){

		return Order.find().then(result =>{
			return result;
		});

	} 	let message = Promise.resolve('User must be an Admin to get all products.')

		return message.then((value)=> {
		return value;
		});
};

// get Check out
module.exports.getCheckOut = async (product, data) => {
  let findOrders = await Order.find({userId : data.userId});

  const result = () => {
    for (let i = 0; i < findOrders.length; i++) {
      let x = findOrders[i].products;
      for (let j = 0; j < x.length; j++) {
        let y = x[j].productId;
        if (y === product) {
          return findOrders[i];
        }
      }
    }
    return false;
  };

  if (findOrders.length > 0) {
    return result();
  } else {
    return false;
  }
};