const express = require("express");
const router = express.Router();
const auth = require("../auth");

// Imports
const userController = require("../controllers/userController");

// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});


// Registration
router.post("/register", (request, response) =>{
	userController.registerUser(request.body).then(result => response.send(result)); 
});

// Login
router.post("/login", (request, response) => {

	userController.loginUser(request.body).then(result => response.send(result));
})

// Check Out

router.put("/checkout/:id", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
	}

	userController.checkOut(request.params.id, data).then(result => {
		response.send(result);
	})
})


// Get User Details

router.get("/getuser", auth.verify, (request, response) => {

	const data = auth.decode(request.headers.authorization)


	userController.getUser(data).then(result => {
		response.send(result);
	})
})

// Get All Users (Admin Only)

router.post("/getalluser", auth.verify, async (request, response) => {

  let data = {
    userId: auth.decode(request.headers.authorization).id
  };

  try {
    const result = await userController.getAllUser(data);
    response.send(result);
  } catch (error) {
    return console.error(error)// handle error
  }

});

// Set User as Admin (Admin Only)

router.put("/setAdmin/:id", auth.verify, (request, response) => {

	const data = {

		admin : auth.decode(request.headers.authorization).id,
		update : request.body
	}

	userController.setAdmin(request.params.id, data).then(result => response.send(result))
});


// Get Authenticated Orders

router.post("/getorders", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id
	}

	userController.getOrders(data).then(result => {
		response.send(result);
	})
})

// Add product to cart

router.post("/addtocart", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
		productId : request.body.productId,
		quantity : request.body.quantity
	}

	userController.addToCart(data).then(result => response.send(result));
})


// Change Product Quantity

router.put("/changequantity", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
		productId : request.body.productId,
		quantity : request.body.quantity
	}

	userController.changeQuantity(data).then(result => response.send(result));
})


// Remove Product from Cart

router.delete("/removefromcart", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
		productId : request.body.productId
	}

	userController.removeFromCart(data).then(result => response.send(result));
})
module.exports = router;