const express = require("express");
const router = express.Router();
const User = require("../models/userModel");
const Product = require("../models/productModel");
const Order = require("../models/orderModel");
const auth = require("../auth");

const productController = require("../controllers/productController");

// Route for adding a product

router.post("/addProduct", auth.verify, (request , response) => {

	const data = {
		product : request.body,
		isAdmin : auth.decode(request.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(result => response.send(result));
});


//Route for getting all products

router.post("/getAllProducts", auth.verify, (request, response) =>{
	
	const data = {
		isAdmin : auth.decode(request.headers.authorization).isAdmin
	}

	productController.getAllProducts(data).then(result => response.send(result));
});

// Route for getting all active products

router.post("/getAllActive", (request, response) => {

	productController.getAllActive().then(result => response.send(result));
});

// Route for getting a specific product

router.get("/:productId", (request, response) => {

	productController.getSingleProduct(request.params).then(result => response.send(result)) ;
	}
);

// For Updating Products

router.put("/updateProduct/:id", auth.verify, (request, response) =>{

	const data = {
		update : request.body,
		isAdmin : auth.decode(request.headers.authorization).isAdmin
	}

	productController.updateProduct(request.params.id, data).then(result => response.send(result));
});

// For Updating Product Status

router.put("/archive/:id", auth.verify, (request, response) =>{

	const data = {
		update : request.body,
		isAdmin : auth.decode(request.headers.authorization).isAdmin
	}

	productController.archive(request.params.id, data).then(result => response.send(result));
});

router.put("/unarchive/:id", auth.verify, (request, response) =>{

	const data = {
		update : request.body,
		isAdmin : auth.decode(request.headers.authorization).isAdmin
	}

	productController.unarchive(request.params.id, data).then(result => response.send(result));
});

module.exports = router;