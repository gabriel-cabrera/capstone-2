const express = require("express");
const router = express.Router();
const auth = require("../auth");

const orderController = require("../controllers/orderController");


// Create Order

router.post("/createOrder/:id", auth.verify, (request, response) => {

	let data = {

		userId : auth.decode(request.headers.authorization).id,
		quantity : request.body.quantity
	}

	orderController.createOrder(request.params.id,data).then(result => response.send(result));

});

// Get user orders

router.get("/getUserOrders", auth.verify, (request, response) => {

	const data = auth.decode(request.headers.authorization)


	orderController.getUserOrders(data).then(result => {
		response.send(result);
	})
})

// Get all orders

router.post("/getAllOrders", auth.verify, async (request, response) => {
	
let data = {
    userId: auth.decode(request.headers.authorization).id
  };

  try {
    const result = await orderController.getAllOrders(data);
    response.send(result);
  } catch (error) {
    return console.error(error)// handle error
  }

});

// get current order
router.get("/getCheckOut/:id", auth.verify, async( request, response) => {

	let data = {
		userId : auth.decode(request.headers.authorization).id
	};

	try {
		const result = await orderController.getCheckOut(request.params.id, data);
		response.send(result)
	} catch (error) {
		return console.error(error) //handles error
	}
})

module.exports = router;