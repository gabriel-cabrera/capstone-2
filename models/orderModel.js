const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId : {
		type : String,
		required : [true, "UserID is required"]
	},
	products : [{
		productId : {
			type : String,
			required : [true, "ProductID is required"]
		},
		name : {
			type : String,
			required : [true, "Product name is required"]
		},
		price : {
			type : Number,
			required : [true, "Product price is required"]
		},
		image : {
			type : String
		}	
	}],
	totalAmount : {
		type : Number,
		required : [true, "Total amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);