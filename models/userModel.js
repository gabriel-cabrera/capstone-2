const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "First Name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last Name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	cart :[{
		products : [{
		productId : {
			type : String,
			required : [true, "ProductID is required"]
		},
		quantity : {
			type : Number,
			default : 1
		},
		subTotal : {
			type : Number,
			required : [true, "Subtotal is required"]
		}
	}],
	totalAmount : {
		type : Number,
		default :  0
	}
	}],
	orders : [{
		orderId : {
			type : String,
			required : [true, "Order ID is required"]
		},
		products : [{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			name : {
				type : String,
				required : [true, "Product Name is required"]
			},
			price : {
				type : Number,
				required : [true, "Price is required"]
			}
		}],
		totalAmount : {
			type : Number,
			required : [true, "Total Amount is required"]
		},
		purchasedOn : {
			type : Date,
			default : new Date()
		}
	}]
})

module.exports = mongoose.model("User", userSchema);