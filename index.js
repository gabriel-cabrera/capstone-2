const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

//Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");


// Connection to Express

const app = express();
const port = 3010;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connection to Mongoose

mongoose.connect(`mongodb+srv://gnrcabrera:${process.env.PASSWORD}@cluster0.enjxqwj.mongodb.net/capstone2-api?retryWrites=true&w=majority`, 
{
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection

db.on("error", console.error.bind(console, "Connection Error!"));
db.on("open", () => console.log("Connected to Server!"));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


// Server Listening
app.listen(port, () => console.log(`App is online on port ${port}!`));